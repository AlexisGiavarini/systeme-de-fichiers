
import java.util.ArrayList;

/**
 *
 * @author Alexis
 */
public class Fichier extends Noeud{
    
    private int taille;
    
    public Fichier(String nom, int taille){
        this.nom = nom;
        this.taille = taille;
        this.id = nombre++;
    }

    @Override
    public boolean ajouteElt(Noeud nouveau) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean supprimeElt(Noeud existant) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList<Noeud> donneElementsFils() {
        return new ArrayList<Noeud>();
    }

    @Override
    public int taille() {
        return this.taille;
    }   

    @Override
    public ArrayList<Noeud> rechercheElt(String nom) {
        ArrayList<Noeud> fichier = new ArrayList<Noeud>();
        
        if(this.nom == nom){
            fichier.add(this);
        }
        
        return fichier;
    }

    @Override
    public Noeud donneParent() {
        return this.parent;
    }

    @Override
    public ArrayList<Noeud> donneChemin(int identifiant) {
        ArrayList<Noeud> fichier = new ArrayList<Noeud>();
        
        if(this.id == identifiant){
            fichier.add(this);
        }
        
        return fichier;
    }

    @Override
    public int donneId() {
        return this.id;
    }
}
