import java.util.ArrayList;

/**
 *
 * @author Alexis
 */
public abstract class Noeud {
    
    protected static int nombre = 0;
    protected int id;
    protected String nom;
    protected Noeud parent; //Je sais c'est mal, c'est juste pour l'exercice
    
    public abstract boolean ajouteElt(Noeud nouveau);
    public abstract boolean supprimeElt(Noeud existant);
    public abstract ArrayList<Noeud> donneElementsFils();
    public abstract int taille();
    public abstract ArrayList<Noeud> rechercheElt(String nom);
    public abstract Noeud donneParent();
    public abstract ArrayList<Noeud> donneChemin(int identifiant);
    public abstract int donneId();    
}
