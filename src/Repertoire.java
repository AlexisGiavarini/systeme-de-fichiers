
import java.util.ArrayList;

/**
 *
 * @author Alexis
 */
public class Repertoire extends Noeud{
    private ArrayList<Noeud> listeNoeuds;
    
    public Repertoire(String nom){
        this.nom = nom;
        this.listeNoeuds = new ArrayList<Noeud>();
        this.id = nombre++;
    }

    @Override
    public boolean ajouteElt(Noeud nouveau) {
        for (int i = 0; i < this.listeNoeuds.size(); i++) {
            if(this.listeNoeuds.get(i).nom == nouveau.nom){
                return false;
            }
        }
        
        nouveau.parent = this;
        
        this.listeNoeuds.add(nouveau);
        return true;
    }

    @Override
    public boolean supprimeElt(Noeud existant) {
        for (int i = 0; i < this.listeNoeuds.size(); i++) {
            if(this.listeNoeuds.get(i).nom == existant.nom){
                this.listeNoeuds.get(i).parent = null;
                this.listeNoeuds.remove(i);
                return true;
            }
        }
        
        return false;
    }

    @Override
    public ArrayList<Noeud> donneElementsFils() {
        return this.listeNoeuds;
    }

    @Override
    public int taille() {
        int tailleTotale = 0;
        if(!this.listeNoeuds.isEmpty()){
            for (int i = 0; i < this.listeNoeuds.size(); i++) {
            tailleTotale += this.listeNoeuds.get(i).taille();
            }
        }
           
        return tailleTotale;
    }

    @Override
    public ArrayList<Noeud> rechercheElt(String nom) {
        ArrayList<Noeud> resultat = new ArrayList<Noeud>();
        
        if(this.nom == nom){
            resultat.add(this);
        }else{
            for (int i = 0; i < this.donneElementsFils().size(); i++) {
                
                if(this.donneElementsFils().get(i).rechercheElt(nom).size() > 0){
                    resultat.addAll(this.donneElementsFils().get(i).rechercheElt(nom));
                }
            }
        }      
        return resultat;
    }

    @Override
    public Noeud donneParent() {
        return this.parent;
    }

    @Override
    public ArrayList<Noeud> donneChemin(int identifiant) {
        ArrayList<Noeud> chemin = new ArrayList<Noeud>();
        
        if(this.id == identifiant){
            chemin.add(this);
        }else{
            for (int i = 0; i < this.donneElementsFils().size(); i++) {
                //this.donneElementsFils().get(i).donneChemin(id);
                
                if(this.donneElementsFils().get(i).donneChemin(identifiant).size() > 0){
                    chemin.add(this.donneElementsFils().get(i).donneParent());
                    chemin.addAll(this.donneElementsFils().get(i).donneChemin(identifiant));                   
                }
            }
        }
        
        return chemin;
    }

    @Override
    public int donneId() {
        return this.id;
    }   
}
